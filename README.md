# Lightburn Test File Generator

These scripts auto-generate lightburn test files for testing speeds
and powers with lightburn. 

This coinsists of a python library for lightburn file generation
(lightburn.py), and a command line script (lbgen.py) that generates
completed test files.

Currently, the library is very limited, but can be extended with many
more features easily enough.

Currently, it can:
   * Create Squares
   * Create Text
   * translate and rotate said squares and text.
   * Set cut settings to either vecotr or raster (Cut/Fill) and set
   the power on each.

That's actually enough to make some pretty useful test files.

## Usage

Let's say we're engraving on a material that we have a reasonable
guess of the power and speed needs, but want to fine tune it.

Create a grid from 30% power to 70% power and speeds from 100 to 300.
Additionally we'll take 5 steps in power and 7 steps in speed.

```$ python lbgen.py --title "Some Engraving" --labeltype=fill --fill --size=5 --space=.5 --speed 100,300,7 --power 30,70,5 grid.lbrn2```

Generates the following:
![](images/engrave1.jpg)

Now, let's walk through each option:

   **--title "Some Engraving"**:  This puts the title at the top.  Completely optional

   **--labeltype=fill**: Sets the label to be a fill layer instead of a cut layer.

   **--fill**: Sets the grid to be fill

   **--size=5**: The x and y size of the squares, in mm

   **--space=.5**: The spacing between the squares is 0.5mm

   **--speed 100,300,7**: Set the speed range from 100 to 300, in 7 steps.

   **--power 30,70,5**: Set the power range from 30 to 70 in 5 steps

The beauty of this is it only takes about 1 second to regenerate with different parameters.  No need to hit some web site, enter all the settings, and re download a file.  Just use your keyboard's up-arrow, edit the command, hit enter, and you're done!

Let's say we messed up, and want the speeds to be much lower.  up-arrow, and edit:

```$ python lbgen.py --title "Some Engraving" --labeltype=fill --fill --size=5 --space=.5 --speed 10,30,7 --power 30,70,5 grid.lbrn2```

and... voila.  About 2 seconds of work!
![](images/engrave2.jpg)

## Log space searches.
Let's say you have really *NO* idea where your power and speed settings are for a new material/process.  In that case, if you use a 10x10 grid from 100 to 500 speed, you're completely missing everything below 100 speed.  Similar questions with power.

Enter, log-spaced searches.  The `--logspeed`, `--logpower`, and `--loglog` options let you search a much wider space, still with only a small grid by spacing the settings logrithmically between min and max.

So, let's say we want to search the entire space between 10mm/s and 500mm/s, and 1% power and 100% power, but still maintain sensitivity distinctions at lower powers and speed.  That's pretty simple:

```$ python lbgen.py --loglog --fill --size=5 --space=.5 --speed 10,500,10 --power 1,100,10 grid.lbrn2```

![](images/loglog.jpg)

The interesting thing here is that it maintains good resolution at the low end, while taking bigger jumps at the higher end.  That is ... power nearly *doubles* each step, rather than adding a constant offset.  

## Command line help
You can always get the latest command line help from:
```
$ ./lbgen.py  --help
usage: lbgen.py [-h] [-s SIZE] [-p SPACE] [--speed SPEED] [--power POWER]
                [--logpower] [--logspeed] [--loglog] [--cut] [--fill]
                [--labeltype {cut,fill}] [--title TITLE]
                OUTPUT

positional arguments:
  OUTPUT                Output file. .lbrn2 will be appended if it's not there
                        already

optional arguments:
  -h, --help            show this help message and exit
  -s SIZE, --size SIZE  Size of each square, in mm. Default=10
  -p SPACE, --space SPACE
                        Spacing between squares, in mm. Default=1
  --speed SPEED         start and stop speeds, and number of speeds to test
                        total. Default='10,500,10
  --power POWER         start and stop power, and number of powers to test
                        total. Default='12,100,10
  --logpower            apply a logrithmic scale to the power settings.
  --logspeed            apply a logrithmic scale to the speed settings
  --loglog              apply a logrithmic scale to both power and speed
  --cut                 cut lines. This is the default
  --fill                fill boxes
  --labeltype {cut,fill}
                        Labels as cut or fill? Defaults to fill
  --title TITLE         Add a title
```

## Installing
Currently, it's assumed you have a working knowledge of the command line, and are able to invoke python.  

You'll need numpy.  I think that's all you need.  Personally, I always use the Anaconda python distribution, but YMMV.


## Library usage
The library is fairly easy to use.

```python
from lightburn import Lightburn, AffineTransform, Square, Text, CutLayer, FillLayer

# Create the lightburn object
lb = Lightburn()

# Set settings for layer 3
lb.add_layer(CutLayer(index=3, name="Layer 3", minPower=0, maxPower=100, speed=35))

# Add a square at the origin with zero rotation, on that layer
lb.add(Square(10, 10).layer(3))

# Add a square at (25, 0)
lb.add(Square(10, 10).layer(3).translate(25, 0))

# Add a bunch of squares at (25, 25), but then rotate them around the point 50, 50.
for angle in range(0, 360, 36):
    lb.add(Square(10, 10).layer(3).translate(25, 25).rotate(50, 50, angle))

# Add some text
lb.add(Text(10, "YAY, TEXT!").layer(3).translate(50, 50))

# And finally, write it out
lb.write("mytest.lbrn2")
```

![](images/example-api.jpg)

## Image data format inside the lightburn file

The Image data format is simply a base64 encoded PNG file.  So, embedding an image is as simple as filling out a shape:
```xml
<Shape
    Type="Bitmap"
    CutIndex="0"
    W="2.540005"
    H="1.6933367"
    Gamma="1"
    Contrast="0"
    Brightness="0"
    EnhanceAmount="0"
    EnhanceRadius="0"
    EnhanceDenoise="0"
    File="whatever" SourceHash="0" Data="<base64-encoded-data>"
    >
    <XForm>1 0 0 1 51.2392 72.1098</XForm>
</Shape>
```

You can create this data from the command line easily enough:
```bash
# convert myfile.jpg myfile.png
# cat myfile.png | base64 > myfile.png.base64
```
Then you simply copy the contents of the base64 file into the Data field above
