# Examples

These are some example files generated by lightburn that I'm using as a template.


* blank.lbrn2:  a blank file.
* square.lbrn2: a single square at 0, 0, sized 10mm x 10mm, on layer 0
* square-rot45: a single square at 0, 0, sized 10mm x 10mm, on layer 0, but rotated 45 degrees
* square-layer1: same as square, but on layer1, and unlinked
* closed-path: a closed path
* open-path: an open path
* text-left-aligned: obvi
* text-middle-aligned: same as left, same position, but setting is different.
* text-right-aligned: same as left, but setting to right aligned.