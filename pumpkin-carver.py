import lightburn as lbrn
import PIL.Image
import numpy as np
import argparse


def get_args():
    p = argparse.ArgumentParser()
    p.add_argument("-s", "--speed", help="speed.  default 100", default=100, type=float)
    p.add_argument("-p", "--power", help="power.  default 100",   default=100, type=float)
    p.add_argument("--clipper-power", help="power of first pass to clear out the hard outer crust.  Default=100", default=100, type=float)
    p.add_argument("--clipper-speed", help="Speed of first pass to clear out the hard outer crust.  Default=200", default=100, type=float)
    p.add_argument("-c", "--clip", help="set to create a first pass that will just carve off the outer layer of maerial.", action='store_true')
    p.add_argument("--levels", help="number of levels to cut.  Default=10", default=10, type=int)
    p.add_argument("IMAGE", help="Input image file.")
    p.add_argument("OUTPUT", help="Output file.  .lbrn2 will be appended if it's not there already")
    args = p.parse_args()
    return args


def main():
    args = get_args()
    lb = lbrn.Lightburn()
    img = PIL.Image.open(args.IMAGE)
    img = PIL.ImageOps.invert(img.convert('L'))
    pix = np.array(img)
    thresholds = np.linspace(0, 255, args.levels+1, endpoint=True)
    thresholds = thresholds[:-1]
    
    lb.add_layer(lbrn.FillLayer(index=0, name=f"clipper", maxPower=args.clipper_power, minPower=args.clipper_power-1, speed=args.clipper_speed))
    e = lbrn.Ellipse(87/2, 87/2, 0, 0, layer=0)
    lb.add(e)
    mask_shape_id = e.get_shape_id()
    for level, threshold in zip(range(args.levels), thresholds):
        layer = args.levels-level + 1
        pass_img = img.point(lambda p: 255 if p > threshold else 0)
        passes = level+1
        lb.add_layer(lbrn.ImageLayer(index=layer, name=f"{passes} pass", maxPower=args.power, minPower=0, speed=args.speed, numPasses=1))
        lb.add(lbrn.Image(100, 100, pass_img).layer(layer).maskID(mask_shape_id))
    lb.reverse_layers()
    lb.reverse_objects()
    lb.write(f"{args.OUTPUT}.lbrn2")

if __name__ == "__main__":
    main()
