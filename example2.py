from lightburn import Lightburn, AffineTransform, Square, Text, CutLayer, FillLayer, Ellipse, Path
import numpy as np

# Create the lightburn object
lb = Lightburn()

# Add a square at the origin with zero rotation, on that layer
#lb.add(Square(10, 10).translate((10, 10)).layer(3))

c = np.array((100, 100))
t = np.array((30, 60))
lb.add(Square(1, 1).translate(c))
#lb.add(Square(10, 10).layer(3))
#lb.add(Ellipse(1, 1).translate(c))
#lb.add(Text(10, f"Hello!!!").translate(t).layer(16))
#lb.add(Text(10, f"Hello10").translate(t).layer(16).rotate(c, 10))
if 1:
    #    for r in range(0, 360, 1):
    for r in np.array([45])/360*2*np.pi:
        a = AffineTransform()
        a.translate(t)
        print(a, a.pos())
        lb.add(Ellipse(3, 3, a.pos()).layer(1))
        a.translate(-c)
        print(a, a.pos())
        lb.add(Ellipse(3, 3, a.pos()).layer(2))
        a.rotate((0, 0), r)
        print(a, a.pos())
        lb.add(Ellipse(3, 3, a.pos()).layer(3))
        a.translate(c)
        print(a, a.pos())
        lb.add(Ellipse(3, 3, a.pos()).layer(4))

#        s2 = Text(10, f"XXX-{r}").translate(t).layer(16).rotate(c,  r)
#        lb.add(s2)
#        lb.add(Ellipse(1, 1).translate(s2.pos()))

#lb.add(Text(10, "Hello-90").translate(t).layer(16).rotate(c, 90))
# for i in range(0, 360, 15):
# for i in [0, 10, 20, 30]:
#    lb.add(Text(10, f"Hello-{i}").translate((50, 20)).rotate(c, i))
#    lb.add(Square(10, 10).layer(3).rotate(c, i))
##
lb.write("mytest.lbrn2")
##
