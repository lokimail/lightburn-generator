from lightburn import Lightburn, AffineTransform, Square, Text, CutLayer, FillLayer
import argparse
import numpy as np


def max_100(x):
    x = int(x)
    if x>0 and x<=100:
        return x
    raise Exception("must be between 0 and 100")

def gt_zero(x):
    x = float(x)
    if x>0:
        return x
    raise Exception("must > 0")

def gt_zero_int(x):
    return int(gt_zero(x))

def get_args():
    p = argparse.ArgumentParser()
    p.add_argument("-x", "--num-x", help="Number of modules in x", default=3, type=gt_zero_int)
    p.add_argument("-y", "--num-y", help="Number of modules in y", default=2, type=gt_zero_int)
    p.add_argument("-s", "--speed", help="cut speed, default=10mm/sec", default=10, type=float)
    p.add_argument("-p", "--power", help="cut power, default=90", default=90, type=max_100)
    p.add_argument("--corner-radius", help="Corner radius of inner squares, default=2", default=2, type=gt_zero)
    p.add_argument("--pitch", help="Gridfinity pitch.  Don't change this.  default=42", default=42, type=float)
    p.add_argument("--opening", help="Gridfinity opening.  Don't change this.  default=38", default=38, type=float)
    p.add_argument("OUTPUT", help="Output file.  .lbrn2 will be appended if it's not there already")

    args = p.parse_args()

    if not args.OUTPUT.lower().endswith(".lbrn2"):
        args.OUTPUT += ".lbrn2"
    return args


def generate_gridfinity(lb, nx, ny, speed, power, pitch, opening, corner_radius):
    offset = pitch/2
    lb.add_layer(CutLayer(index=0, name="outline", minPower=power, maxPower=power, speed=speed))
    lb.add_layer(CutLayer(index=1, name="cutouts", minPower=power, maxPower=power, speed=speed))

    x, y = nx*pitch, ny*pitch
    lb.startgroup()
    lb.add(Square(x, y, Cr=corner_radius).layer(0).translate(x/2, y/2))
    lb.startgroup()
    for col in range(nx):
        for row in range(ny):
            lb.add(Square(opening, opening, Cr=corner_radius).layer(1).translate(col*pitch+offset, row*pitch+offset))
    lb.endgroup()
    lb.endgroup()
    
    
def main():
    args = get_args()
    lb = Lightburn()
    generate_gridfinity(lb=lb,
                        nx=args.num_x, ny=args.num_y,
                        speed=args.speed, power=args.power,
                        pitch=args.pitch, opening=args.opening, corner_radius=args.corner_radius)
    lb.write(args.OUTPUT)

if __name__ == "__main__":
    main()
