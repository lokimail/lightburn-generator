#!/usr/bin/env python
from lightburn import Lightburn, AffineTransform, Square, Text, CutLayer, FillLayer
import argparse
import numpy as np


def speed_range(x):
    start, stop, n = x.split(",")
    start = float(start)
    stop = float(stop)
    n = int(n)
    if start <= 0 or start >= 5000 or stop <= 0 or stop >= 5000 or n <= 0:
        raise Exception("Speed is out of range. {start}, {stop}, {n}")
    return (start, stop, n)

def power_range(x):
    x = speed_range(x)
    if x[0] > 100 or x[1] > 100:
        raise Exception("power is out of range. x")
    return x
    
def get_args():
    p = argparse.ArgumentParser()
    p.add_argument("-s", "--size", help="Size of each square, in mm.  Default=10", default=10., type=float)
    p.add_argument("-p", "--space", help="Spacing between squares, in mm.  Default=1", default=1., type=float)
    p.add_argument("--speed", help="start and stop speeds, and number of speeds to test total.  Default='10,500,10", default=[10., 500., 10], type=speed_range)
    p.add_argument("--power", help="start and stop power, and number of powers to test total.  Default='12,100,10", default=[12., 100., 10], type=power_range)
    p.add_argument("--logpower", help="apply a logrithmic scale to the power settings.", action='store_true')
    p.add_argument("--logspeed", help="apply a logrithmic scale to the speed settings", action='store_true')
    p.add_argument("--loglog", help="apply a logrithmic scale to both power and speed", action='store_true')
    p.add_argument("--cut", help="cut lines.  This is the default", action='store_true')
    p.add_argument("--fill", help="fill boxes", action='store_true')
    p.add_argument("--labeltype", help="Labels as cut or fill?  Defaults to fill", choices = ["cut", "fill"], default="fill")
    p.add_argument("--title", help="Add a title", default=None)
    p.add_argument("OUTPUT", help="Output file.  .lbrn2 will be appended if it's not there already")

    args = p.parse_args()

    print(args)
    if not args.OUTPUT.lower().endswith(".lbrn2"):
        args.OUTPUT += ".lbrn2"
    
    args.cut = True
    if args.fill:
        args.layer_type = FillLayer
    else:
        args.layer_type = CutLayer

    if args.labeltype == "cut":
        args.label_layer_type = CutLayer
    elif args.labeltype == "fill":
        args.label_layer_type = FillLayer
    else:
        raise Exception(f"Unknown layer type {args.labeltype}")
        
    if args.loglog:
        args.logpower = True
        args.logspeed = True

    if args.fill:
        args.cut = False
    
    if args.speed[2] > 29:
        print(f"There are too many speeds:  you asked for {args.speed[2]}.  Lightburn is limited to 30 different layers, and therefore 29 different speeds.")
        exit(-1)
    return args


def generate_grid(lb, speed_range, power_range, size, space, logspeed, logpower, layer_type, label_layer_type, title):
    nrows=speed_range[2]
    ncols=power_range[2]
    def make_space(the_range, is_log):
        start, stop, n = the_range
        if is_log:
            return np.logspace(np.log10(start), np.log10(stop), n, base=10.0, endpoint=True)
        else:
            return np.linspace(start, stop, n, endpoint=True)

    for row, speed in enumerate(make_space(speed_range, logspeed)):
        lb.add_layer(layer_type(index=row, name=f"{int(speed)}mm/s", minPower=0, maxPower=100, speed=speed))
        for col, power in enumerate(make_space(power_range, logpower)):
            lb.add(Square(size, size).power(power).layer(row).translate(col * (size+space), row * (size+space)))

    textlayer=nrows
    lb.add_layer(label_layer_type(index=textlayer, name="Text", minPower=50, maxPower=50, speed=100))
    for row, speed in enumerate(make_space(speed_range, logspeed)):
        x, y = (ncols) * (size+space)- space, row * (size+space) - 0.25*size
        lb.add(Text(size, f"{speed:3.0f}").layer(textlayer).translate(x, y))

    for col, power in enumerate(make_space(power_range, logpower)):
        if power >= 100:
            lb.add(Text(size, f"{99}%").layer(textlayer).rotate(0, 0, 270).translate(col * (size+space), -1*size))
        else:
            lb.add(Text(size, f"{power:2.0f}%").layer(textlayer).rotate(0, 0, 270).translate(col * (size+space), -1*size))
    
    lb.add(Text(size*.75, "PWR").layer(textlayer).rotate(0, 0, 270).translate(-size*.75, -1*size))
    lb.add(Text(size, "SPD").layer(textlayer).translate(ncols * (size+space)- .5*space, -2.5*size))
    lb.add(Text(size*.5, "(mm/s)").layer(textlayer).translate(ncols * (size+space)- .5*space, -1.5*size))
    if title is not None:
        lb.add(Text(size, title).layer(textlayer).translate(0, -4*size))
    
def main():
    args = get_args()
    lb = Lightburn()
    generate_grid(lb, args.speed, args.power, args.size, args.space, args.logspeed, args.logpower, args.layer_type, args.label_layer_type, args.title)
    lb.write(args.OUTPUT)

if __name__ == "__main__":
    main()
