from lightburn import Lightburn, AffineTransform, Square, Text, CutLayer, FillLayer, Ellipse, Path

# Create the lightburn object
lb = Lightburn()

# Set settings for layer 2
lb.add_layer(CutLayer(index=3, name="Layer 3", minPower=0, maxPower=100, speed=35))
# Set settings for layer 3
lb.add_layer(FillLayer(index=3, name="Layer 2", minPower=0, maxPower=100, speed=35))

# Add a square at the origin with zero rotation, on that layer
lb.add(Square(10, 10).layer(3))

# Add a square at (25, 0)
lb.add(Square(10, 10).layer(3).translate(25, 0))

# Add a bunch of squares at (25, 25), but then rotate them around the point 50, 50.
for angle in range(0, 90, 10):
    lb.add(Square(10, 10).layer(3).translate(25, 25).rotate(50, 50, angle))

# Add some text
lb.add(Text(10, "YAY, TEXT!").layer(3).translate(50, 50))

# Add a circle
lb.add(Ellipse(5, 2.5).layer(2))

# Add a path
lb.add(Path([[10, 10], [10, 20], [19, 19], [12, 8], [10, 10]]).layer(1))

# Let's add a more complex path, and copy-paste it around a few times
from a_shape import shape, important_vertexes

#lb.add(Path(shape).layer(0))
#for vi in important_vertexes[:1]:
#    v = shape[vi]
#    print(v)
#    lb.add(Path(shape).layer(0).rotate(*v, 45))
#    lb.add(Path(shape).layer(0).rotate(*v, 90))
#    lb.add(Square(1, 1).translate(*v))
#    

# And finally, write it out
lb.write("mytest.lbrn2")
